let colors = [
  'aliceblue', 'antiquewhite', 'aqua', 'aquamarine',
  'azure', 'beige', 'bisque', 'black', 'blanchedalmond', 'blue',
  'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse',
  'chocolate', 'coral', 'cornflowerblue', 'cornsilk', 'crimson',
  'cyan', 'darkblue', 'darkcyan', 'darkgoldenrod', 'darkgray',
  'darkgreen', 'darkkhaki', 'darkmagenta', 'darkolivegreen',
  'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen',
  'darkslateblue', 'darkslategray', 'darkturquoise', 'darkviolet', 'deeppink',
  'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'floralwhite', 'forestgreen',
  'fuchsia', 'gainsboro', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green',
  'greenyellow', 'honeydew', 'hotpink', 'indianred', 'indigo', 'ivory', 'khaki',
  'lavender', 'lavenderblush', 'lawngreen', 'lemonchiffon', 'lightblue', 'lightcoral',
  'lightcyan', 'lightgoldenrodyellow', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon',
  'lightseagreen', 'lightskyblue', 'lightslategray', 'lightsteelblue', 'lightyellow', 'lime',
  'limegreen', 'linen', 'magenta', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid',
  'mediumpurple', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise',
  'mediumvioletred', 'midnightblue', 'mintcream', 'mistyrose', 'moccasin', 'navajowhite', 'navy',
  'oldlace', 'olive', 'olivedrab', 'orange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen',
  'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink', 'plum', 'powderblue',
  'purple', 'red', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen', 'seashell',
  'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'snow', 'springgreen', 'steelblue', 'tan',
  'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'wheat', 'white', 'whitesmoke', 'yellow', 'yellowgreen'
];
let userInput = document.getElementById("userInput");
let textDisp = document.getElementById("textDisp");
let userImg = document.createElement("img");
userImg.style.display = "inline-block;"
userImg.width = 500;
userImg.style.borderRadius = "5px";
let userIFrame = document.createElement("iframe");
// userIFrame.width = 0;
// userIFrame.height = 0;
userIFrame.frameBorder = 0;
userIFrame.allowFullscreen = true;
userIFrame.style.marginLeft = "-15px";
let youtubeProperties = "?showInfo=0&autoplay=1&rel=0&loop=1&modestbranding=1&cc_load_policy=1&iv_load_policy=3";
let textColor = "black";
let bgColor = "white";
let origBackgroundColor = "rgb(77, 109, 109)";
let currentBackgroundColor = origBackgroundColor;
let isMobile = function () {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
    return true;
  } else {
    return false;
  }
}

let checkIfIsMobile = setInterval(function () {
  userIFrame.width = document.documentElement.clientWidth;
  userIFrame.height = document.documentElement.clientHeight * 0.666;
}, 1000);


function hideBorderOnInput() {
  if (userInput.value != "") {
    userInput.style.border = "0px";
  } else {
    userInput.style.borderBottom = "2px solid white";
  }
}

function setTextDisp(value) {
  textDisp.innerHTML = value;
}

function isColor(string) {
  let isName = colors.includes(string);
  let isHex = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/.test(string);
  let isRGB = /rgb\(([01][0-9]?[0-9]?|2[0-4][0-9]|25[0-5]),[\s]?(\d{1,3}),[\s]?(\d{1,3})\)/.test(string);
  return isName || isHex || isRGB;
}

function dispUserInput(id, url) {
  // PAGE COLOR
  setTextColorBasedOnBackgroundColor(textColor, bgColor);

  if (isColor(userInput.value)) {
    pageBackgroundColor(userInput.value);
    currentBackgroundColor = userInput.value;
  } else if (userInput.value.toUpperCase() === "RESET") {
    pageBackgroundColor(origBackgroundColor);
  }

  // DISPLAY INPUT BASED ON WORD
  if (userInput.value.toUpperCase() === id.toUpperCase() && url.includes("www.youtube.com")) {
    userIFrame.src = url + youtubeProperties;
    document.getElementById("contentDisp").appendChild(userIFrame);
  } else if (userInput.value.toUpperCase() === id.toUpperCase() && !url.includes("www.youtube.com")) {
    addImage(url);
  } else if (userInput.value.toUpperCase() != id.toUpperCase() && userImg.src === url && !url.includes("www.youtube.com")) {
    if (document.getElementById("contentDisp").hasChildNodes()) {
      userImg.src = "";
      document.getElementById("contentDisp").removeChild(userImg);
    }
  } else if (userInput.value.toUpperCase() != id.toUpperCase() && userIFrame.src === url + youtubeProperties && url.includes("www.youtube.com")) {
    if (document.getElementById("contentDisp").hasChildNodes()) {
      userIFrame.src = "";
      document.getElementById("contentDisp").removeChild(userIFrame);
    }
  }

  // DISPLAY INPUT BASED ON URL
  else if ((userInput.value.includes("www.") ||
      userInput.value.includes("http") ||
      userInput.value.includes(".png") ||
      userInput.value.includes(".jpg") ||
      userInput.value.includes(".jpeg") ||
      userInput.value.includes(".svg") ||
      userInput.value.includes(".gif")) &&
    !url.includes("www.youtube.com")) {
    userImg.src = userInput.value;
    document.getElementById("contentDisp").appendChild(userImg);
  }
}

function addImage(url) {
  userImg.src = url;
  document.getElementById("contentDisp").appendChild(userImg);
}

function pageBackgroundColor(color) {
  document.body.style.backgroundColor = color;
  document.getElementsByTagName("html")[0].style.backgroundColor = color;
  userInput.style.backgroundColor = color;
}

function pageTextColor(color) {
  document.getElementById("hi").style.color = color;
  userInput.style.color = color;
}

function setTextColorBasedOnBackgroundColor(text, bg) {
  if (document.body.style.backgroundColor === bg) {
    if (userInput.value === "") {
      userInput.style.borderColor = text;
    }
    pageTextColor(text);
  } else {
    pageTextColor(bg);
  }
}